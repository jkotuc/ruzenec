## Nová verze webu

Tato webová aplikace nepodporuje ukládání úmyslů, jejich rozesálání, ani upozorňování toho, kdo je na řadě s jeho určením. Toto je doporučené udělat přes nějaký sdílený kalendář (Google kalendář a další).

### Starý web

Skupinky z předchozího webu byly do nové verze převedy a najdete je pod záložkou [Seznam skupin](/seznam skupin/).

### Plán do budoucna

- Sepsat návod na používání nové webové aplikace.
- Připravit jednoduchá tlačítka na vytvoření událostí v mém kalendáři.

## Kontakt

[info@zivyruzenec.cz](mailto:info@zivyruzenec.cz)
